
##############
Mounting guide
##############


Mounting Devices
================

In Linux, all of the drives, partitions and devices in your
computer are treated as "files". Various hard drive partitions,
CD-ROMs, and floppies are all placed in the same filesystem tree.
In order to be able to access the contents of those devices,
external or internal devices, they must be "mounted".

The default installation of VectorLinux v6.0 enables the
`HAL automounting system <http://en.wikipedia.org/wiki/HAL_(software)>`_.
HAL detects newly attached devices such as USB devices, floppy disk
, CD-DVD disks, etc. It then automounts them and then launches an
appropriate application. It does this seamlessly in KDE and in
xfce. In other window managers provided with VL 6.0 you will need
to open the PCManFM file manager and check its right hand panel for
your device and double click that device to get access to it.
`vl-hot <http://broadcast.oreilly.com/2009/02/vl-hot-a-non-polling-alternati.html>`_
is similar to HAL, but uses the udev system and is much less
resource intensive. It would be more appropriate to use in a lower
resource older computer, and especially with the VectorLinux Light
Edition. At present though, vl-hot only works with JWM and IceWM as
long as the PCManFM file manager is also installed (this is the
case with VL Light 6.0).

With either HAL or vl-hot working properly inserting external
devices such as any USB device, a floppy disk or a CD-DVD platter
will cause them to be automounted and an appropriate application
will launch or a new icon for accessing the device will appear on
your desktop. However, if you use a desktop other than those listed
in the paragraph above, add a new hard drive or want a device to be
automounted whenever you reboot the computer then you will need to
familiarize yourself with the Linux "mount" and "umount" commands
and also learn about the mount configuration file: /etc/fstab. The
rest of this document explains these concepts. Newbies probably do
not need to concern themselves yet with these concepts.

The easiest way to mount a device such as a second hard disk or
partition is by adding a permanent entry in the */etc/fstab* file.
In this manner the device will be automatically mounted for you at
every boot.

.. note:: If you are not using HAL or vl-hot and you therefore do
    not know what device name has been assigned to the USB device you
    have just inserted then the easiest way to determine the device
    name is to open a terminal immediately after you have inserted the
    device and type "tail -f /var/log/messages". The second or third
    last line of the file should tell you the device name.


Mounting devices with VASM
==========================

VASM provides an easy method to configure additional local disks or
partitions to be mounted in your system. Just launch it, select
"FILESYSTEM", then "MOUNT", and there you can ADD or REMOVE mount
points.

**- Adding mount points**

    If you select "Add" you will be presented with a list of the local
    filesystems which are available and which are not already present
    in the fstab file (the file where mount points configuration is
    stored).

    Select the one you want to configure and you will be asked where
    you want it to be mounted. Then you will see the default options it
    will be mounted with. If you know what you are doing, you can
    modify these options, otherwise accept the defaults and press
    "Ok".

    The filesystem will not be automatically mounted, you will have to
    mount it manually by either command line, Kwikdisk, or utilities
    such as gkrellm.

    If you mount it by command line, and assuming you created a mount
    point in /mnt/win, you could use this command (as root):

    ``mount /mnt/win``
    You could also use:

    ``mount -a``
    this will mount *all* of the mount points available in the
    configuration file, */etc/fstab*.

.. note:: Keep in mind that the default settings make the
    filesystem mountable ONLY by the root user and not by ordinary
    users. If you want to make it mountable by users you could append
    "users," to the default options that are presented during the
    process.


**- Deleting mount points**

    When you delete a mount point, all you are doing is telling the
    system not to access some particular filesystem (disk, partition,
    etc,), you are not actually deleting any contents of that
    filesystem.

    So, if you choose to Delete a mount point, you will see a list of
    the devices configured to be mountable (not necessarily
    auto-mounted). Simply select those you want to remove and press
    "Ok". Their entries will be removed from the configuration files
    and nothing will be modified in the filesystem itself.

    **Notes:** It is highly recommended that you unmount the filesystem
    BEFORE you delete it's mount point. If you do not unmount the
    filesystem before, it will not be unmounted automatically, so you
    will have to do it either by command line, Kwikdisk, gkrellm or by
    rebooting the system.


The fstab file
==============

Since VL5, using VASM is all you need to know to mount your
partitions or hardisks. However, for those wanting to know more
about mounting devices, here is some additional information.
Let's look at an example of the */etc/fstab* file::

    <device>    <mount point>   <type>      <options>
    /dev/hda3   /               ext3        defaults 1 1
    /dev/hda2   swap            swap        defaults 0 0
    /dev/hda1   /mnt/windows    vfat        defaults,noauto 0 0
    /dev/hda4   /mnt/personal   vfat        defaults,auto,umask=000 0 0
    none        /dev/pts        devpts      gid=5,mode=620 0 0
    none        /proc                       defaults 0 0
    /dev/fd0    /mnt/floppy     msdos       defaults 0 0
    /dev/cdrom   /mnt/cdrom      iso9660     ro 0 0

The first column is the device name. In this case, the devices are
four partitions in an IDE hard drive, two special filesystems that
don't need a device, a floppy, and a CD-ROM drive. The second
column is where in the directory tree the device will be mounted.
This needs to be a directory name, except in the case of a swap
partition. The third column is the filesystem type of the device.
For Windows 9x filesystems, this will be vfat, for normal Linux
filesystems, this will be ext2 (second extended filesystem). CD-ROM
drives are iso9660, and other Windows-based devices will either be
msdos or vfat. NTFS filesystems read support is available but write
support is not fully reliable yet.

The umask section deals with access permissions to FAT devices.
This is a very complex topic that goes beyond the scope of this
guide, but keep in mind that the 000 option poses almost no
restrictions about how regular users can access the device.

The last column is a listing of options that apply to the mounted
filesystem. “defaults” is fine for just about everything. However,
read-only devices should be given the ro flag. There are a lot of
options that can be used. Check the fstab(5) man page for more
information. The last two columns are used by fsck and other
commands that need to manipulate the devices. Check the man page
for that information, as well.

When you install VectorLinux, the setup program will build much of
the fstab file. The only time you will need to edit it is if you
add disks or want devices to be automatically mounted at boot
time.


Mount and umount commands
=========================

Attaching another device to your filesystem is easy. All you have
to do is use the mount command, along with a few options.
**Using mount can also be made much more simple if the device has an entry in the /etc/fstab file.**
For example, let's say that I wanted to mount my CD-ROM drive and
that my fstab file looked like the example from the previous
section. I would call mount like so:

``# mount /mnt/cdrom``
Since there is an entry in fstab for that mount point, mount knows
what options to use. If there wasn't an entry for that device, I
would have to use several options for mount:

``# mount -t iso9660 -o ro /dev/cdrom/cdrom``
That command line includes the same information as the example
fstab did, but we'll go over all the parts anyways. The -t iso9660
is the filesystem type of the device to mount. In this case, it
would be the iso9660 filesystem which is what CD-ROM drives most
commonly use. The -o ro tells mount to mount the device read-only.
The /dev/cdrom is the name of the device to mount, and /mnt/cdrom
is the location on the filesystem to mount the drive.

Before you can remove a floppy, CD-ROM, or other removable device
that is currently mounted, you'll have to unmount it. That is done
using the ``umount`` command. You can use either the mounted device
or the mount point as the argument to umount. For example, if you
wanted to unmount the CD-ROM from the previous example, either of
these commands would work:

``# umount /dev/cdrom``
``# umount /mnt/cdrom``

In the default configuration of VectorLinux, only the root user is
able to mount and umount devices. You can allow regular users the
ability to mount devices by adding "users" (without quotes) in your
fstab file::

    /dev/hda4   /mnt/personal   vfat    defaults,auto,users,umask=000 0 0

