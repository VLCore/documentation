===================================
|vector_edition| Contributing Guide
===================================

This document will explain how to contribute SlackBuilds to the VectorLinux
project.  It will also document the suggested workflow for introducing new
packages into the main repositories as well as package updates for currently
supported packages within the Vector release model.


The |vector_edition| Release Model
==================================

|vector_edition| **does not follow a rolling release** model.  An effort is made to
provide up-to-date software as much as possible, but the main goal when updating
software to make sure it will not break any of the existing system configuration
or conflict with other packages.

All updates are tested and introduced during the developmental periods.  As
most projects, VectorLinux has 3 development stages before the final product is
shipped out.

- Alpha
  
  During this stage, contributors are free to update as many of the components
  as they want, except the core packages that make up the build base and toolchain.
  At this stage, the development team is trying to determine what should become
  part of the final product, which packages to include, or take out.

- Beta
  
  During this stage, the team has settled on a particular set of packages that will
  make up the final product.  The development focus is shifted to resolving any
  issues with the current set of packages, like setting defaults, debugging builds,
  etc.  At this time, version updates for the existing packages should slow down and
  only happen if the update fixes an existing issue with the current set of packages.

- RC (Release Candidate)
  
  At this time, the team has fixed most known issues, and present this as a possible
  final product.  At this time, there should be absolutely no more version updates to
  any packages in the ISO.  Development is focused on making the existing set of
  packages work within the current environment and finalizing default desktop
  configurations and user experience.

- FINAL
  
  This indicates the release has reached its final state.  After this is publically
  released, there will be no more version updates to the packages that make up the
  final product, except for security fixes.  The distro will continue to receive
  security updates until it reaches EOL.

The preferred update methos is that version updates to existing packages should not
be introduced unless they are heavily tested within the existing set of packages.

After the final release has been made publically available, the following workflow
can be used to submit contributions for updates.


Contribution Workflow
=====================

Fork -> Clone -> Change -> Commit -> Push -> Issue Pull Request.

Here is the explanation.

- Fork
  
  Visit the git repository for the project (usually at bitbucket or vlcore.vectorlinux.com)
  Once there, you will need to 'fork' the git repository.  This will give you a copy of
  the current set of scripts that build the entire distribution.
- Clone
  
  You will clone your 'fork' of the project.  This is your fork, so you can change it as
  much as you want within your account.  This is done with ``git clone URL``
- Change
  
  Make the changes you want.  For example, you may want to submit an update to the ``firefox``
  package.  So, you update the build script to reflect your update.
- Commit
  
  Commimt your changes to your fork using ``git commit -m <your_commit_message>``
- Push
  
  Push your commited changes to your fork using ``git push``
- Issue Pull Request
  
  Visit your fork of the project online and issue a ``Pull Request`` or ``Merge Request``.
  This will notify the developers you are proposing an update and they will review your
  proposed changes.  This also gives them the oportunity to test your changes before
  commiting them to the final destination.  At this stage, they will most likely comment
  on your changes, communicate with you on what is needed before it can be approved.

  If a fix is needed, you then as a contributor will need to make the necessary changes,
  commit, and push.  Your existing Pull Request will be automatically updated and devs will
  be notified of the updates.

  A reviewer can decide to decline a Pull Request if it is clear that it introduces problems
  to other users or conflicts with other existing packages, or breaks something else within
  the release.


Reviewing Pull Requests
=======================

If you are a developer reviwing a pull request, you should be testing the proposed change
to make sure it plays well with the rest of the system.  All tests should be performed in
a clean environment.  This means you will have to do the following in some shape or form.

- Fire up a sandbox of the target release (``vlsandbox`` may help).
- Add the contributors repo as a remote to vabs
  ``git remote add <name> <url>``
- Pull from the contributors remote
  ``git pull <origin_name> <branch>``
- Run ``vldepper`` on the contributed/updated SlackBuild
- Run the contributed/updated SlackBuild  


When it passes
--------------

When the build passes you should test the application to make sure it works as expected.
If all is well, then you approve the Pull Request and merge it into the main vabs tree.


When it fails
-------------

When your build of the proposed change fails or your test of the built application reveals
a problem with the updated application or conflict with something else, then you should
notify the contributor of the problem.  This will be done by visiting their Pull Request
and commenting on it.  When commenting, be sure to indicate what adjustments are necessary
to make this pull request work.  If you must decline the pull request, the comment
should indicate the reason for the decline.
