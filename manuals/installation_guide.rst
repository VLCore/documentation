
##################
Installation Guide
##################


Getting Started
===============

For the users convenience and choice VectorLinux releases several
editions - called the Standard, SOHO, Deluxe, Live and Light
editions. The Standard Edition is our "foundation" distribution - a
fast and stable but complete distro that fits on ~3GB of hard drive
space and includes the xfce desktop environment. It will work well
on most older computers and positively flies on newer ones. The
SOHO Edition (Small Office, Home Office) includes the more
extensive KDE desktop environment along with OpenOffice and many
office and multimedia applications for today's modern computers.
The Deluxe Editions are available for purchase in Standard or SOHO
CD's along with a second CD with 1000MB of extra applications
including FreeRock Gnome, KDE, Enlightenment17, Opera and others.
You help support VectorLinux by purchasing our Deluxe Versions. The
Live version enables you to use VL's fast, secure operating system
on any computer, or to try out VectorLinux for the first time
without actually installing. The Light edition is for those with
old computers, low RAM or dialup Internet. Each edition has
specific hardware requirements. Please ensure you have read the
requirements for the edition you are about to install before
proceeding. The good news is that the installation procedure for
all editions is basically the same and is fast.


System Requirements
===================

The following table lists system requirements for the various
editions of VectorLinux:

+---------------+-------------+-------------+-------------+-------------+------------+
|               | Standard    | SOHO        | Deluxe      | Live        | Light      |
+===============+=============+=============+=============+=============+============+
| Processor     | Pentium III | Pentium IV  | Pentium IV  | Pentium III | Pentium II |
+---------------+-------------+-------------+-------------+-------------+------------+
| Memory (min.) | 128 MB      | 256 MB      | 512 MB      | 512 MB      | 64 MB      |
+---------------+-------------+-------------+-------------+-------------+------------+
| Disk (root)   | >3.4 GB     | >3.8 GB     | >5 GB       | --          | >2 GB      |
+---------------+-------------+-------------+-------------+-------------+------------+
| Disk (swap)   | 256 MB      | 512 MB      | 512 MB      | --          | 128 MB     |
+---------------+-------------+-------------+-------------+-------------+------------+
| Disk (home)   | as needed   | as needed   | as needed   | --          | as needed  |
+---------------+-------------+-------------+-------------+-------------+------------+
| Video         | 800x600x16  | 1024x768x24 | 1024x768x24 | 1024x768x24 | 800x600x16 |
+---------------+-------------+-------------+-------------+-------------+------------+

Of course, you will also need a compatible keyboard, mouse, and a
CDROM drive. Other hardware components such as a network card,
modem, sound card, CD-writer, DVD, printer, etc., are optional.
Linux also supports modern USB devices including pen drives,
digital cameras, and scanners.


Understanding Hard Drive Partitions
===================================

For newbies this is probably the most difficult but also the most
important concept to learn before installing *any* operating system
on a computer. Before an operating system or data can be stored on
a hard drive that drive must be partitioned and then each partition
must be formatted for a specific filesystem. A partition is a
physical or logical "part" of the hard drive. A filesystem is a
storage container and "database" for storing files. For more
details about partitions please
`read this <http://en.wikipedia.org/wiki/Disk_partitioning>`_ and
for details on Linux filesystems read
`this article <http://www.linux.org/lessons/advanced/x1254.html>`_.

In the Microsoft Windows or default Ubuntu Linux install process
usually only one partition and one filesystem encompassing the
entire hard drive is created. In MS Windows this process tends to
be hidden from the user. So when you install MS Windows (or if you
have a computer with only MS Windows pre-installed) it tends to
wipe out any other partitions that may have existed on your drive.
On the other hand VectorLinux and many other Linux distros provide
you with the option and ability to delete, save, create or resize
partitions and to format those partitions with different
filesystems. This enables you to have more than one operating
system on one hard drive (i.e. "dual boot") and enables easier and
more secure backups.

If you intend to dual boot your computer with both MS Windows and
VL you should install Windows first. This is because in its
narcissistic presumptions Windows takes over the entire hard drive.
If you intend to install Windows after VL then you will need to do
the following:


#. use the "gparted" application from a Live Linux CD to reduce the
   size of your "/home" partition
#. install MS Windows to the now free space on your hard drive
#. re-run the lilo or grub boot loader install process from the VL
   Install CD to overwrite the Windows master boot record (MBR). Lilo
   or Grub will find the Windows partition and add it to the boot
   menu.

Assuming you already have only MS Windows on your hard drive the
entire drive will have already been dedicated to Windows (probably
denoted as C:). In order to install VL on the same hard drive which
already contains a MS Windows operating system you must reduce the
size of the Windows partition to give you room to install Linux.
The easiest way to do this is to use the
`gparted <http://en.wikipedia.org/wiki/Gparted>`_ application from
`Live Linux CD <http://en.wikipedia.org/wiki/Live_CD>`_. Almost all
Live Linux CD's (including VectorLinux LIVE) contain the gparted
application. Documentation for gparted is available from its Help
Menu or online
`here <http://gparted.sourceforge.net/documentation.php>`_.

If you are simply installing VL and VL alone and you want to take
over the entire hard drive for VL then you do not need to do any of
the above. The VL install CD includes partitioning software that
can do partitioning during the install process.

Partitions Recommended for VectorLinux
--------------------------------------

For an efficient Linux install the developers at VectorLinux
recommend that when you install VL you consider creating the
following partitions:


-  Root partition: this is the main partition to install the Linux
   system and all software programs. See the table above for our
   recommended minimum root partition sizes for each VL version. You
   should also include additional space to install other applications
   at a later date. As a rough guideline make your root partition at
   least 5GB in size.
-  Swap partition: required to enable virtual memory. It expands
   the capacity of your actual random access memory (RAM), so that you
   can run more programs at the same time. It should be twice the size
   of your actual computer's RAM, up to a maximum of about 1GB.
-  Home partition: for storing your own data. Only you ca determine
   how large this should be. If you intend to store lots of music and
   video files it will need to be at least 10GB in size, probably much
   larger. Normally, most users use up as much of the remaining hard
   drive space (after creating "root" and "swap") as possible for
   their home partition. The advantage of having a /home partition
   distinct from root ("/") is that at any time you can simply image
   (ghost) the /home partition to preserve your data files and you can
   also install a new version of VL without overwriting all your setup
   files and data.

You may prepare these partitions prior to the installation by using
partitioning software (such as gparted) or you may actually prepare
your hard disk partitions during the install process (this latter
method is easier and is recommended for all newbies). You should at
least know the partition sizes you want for root, home and swap
beforehand (see above).

Linux partition naming scheme
-----------------------------

MS Windows presents hard drive partitions to the user as: C:, D:.
E:, etc. However, Unix/Linux uses a different notation. Firstly, a
computer may have more than one hard drive. Linux maps each hard
drive as a device. For example:


/dev/hda
    primary controller Master IDE drive.
/dev/hdb
    primary controller Slave IDE drive.
/dev/hdc 
    secondary controller Master IDE drive.
/dev/hdd
    secondary controller Slave IDE drive.
/dev/sda
    first SATA/SCSI device.
/dev/sdb
    second SATA/SCSI device.

If you have only one IDE hard drive, it is almost certainly
"/dev/hda". The second hard drive could be /dev/hdb or /dev/hdc,
depending on which controller it is installed.
Next, each hard drive can be divided into four PRIMARY partitions.
For the first hard drive (/dev/hda), they are mapped as /dev/hda1
.. /dev/hda4, respectively. But what if you want more than four
partitions?

Unfortunately, four is a legacy limit you can do nothing about. The
work-around is that one of the primary partitions (/dev/hda2 ..
/dev/hda4) can be used as an EXTENDED partition. Inside the
extended partition, you may create more LOGICAL partitions. The
logical partitions are named /dev/hda5, /dev/hda6 and so on.

All SATA drives will be labelled as "/dev/sda" and for the second
drive as "/dev/sdb", ... etc.


Obtaining VectorLinux
=====================

Each edition of VectorLinux is distributed as an ISO (.iso) "image"
file. To obtain it and prepare it for installation the steps are:


-  Decide which edition of VectorLinux you want. Download the ISO
   image file (.iso) and the corresponding md5 checksum file (.md5).
   The download sites are listed at the
   `VectorLinux website, download page <http://vectorlinux.com/downloads>`_
   and also in the `Overview Document <../index.html>`_.
-  You should check the image file before burning it to a CD, to
   make sure it has not been corrupted during download transmission.
   For that, you need to do an "md5sum check", that means comparing
   the "fingerprint" of your image file (.iso) against the fingerprint
   stored in the checksum file (.md5).
-  Note that VL allows you to install the .iso file directly
   without actually burning to a CD first (see Install Methods below),
   so if you do not wish to burn a CD you don't have to. If you do
   burn the image onto a CD using a CD-writer and ensure that you burn
   the .iso image as an IMAGE, not as a data file. If you burn the
   .iso as a data file you will not be able to boot your new CD. Check
   your CD burning software documentation on how to burn as an
   "image".

If you don't have a good, fast Internet connection, or a CD-writer,
you may order a well-prepared CD from
`our store <http://vectorlinux.com/cd-store>`_.

Checksum Tools for MS Windows
-----------------------------

You may download the ISO image and MD5 checksum using any web
browser, FTP client, or download manager. However, some web
browsers (*Internet Explorer*, for instance) have a tendency to
rename .md5 files as .htm or .txt. You can just rename it back to
.md5.

For integrity checking, download and unzip the GUI tool
`md5summer <http://www.md5summer.org/>`_. On initial use it asks
permission to associate the extension .md5 with itself. If you
agree, you just need to double-click on a .md5 file to check the
integrity of the original file (as long as they are both in the
same folder). Otherwise, you have to manually browse to the .md5
file within the md5summer interface, then click on the "Verify
sums" button and select the .md5 file. If you get an OK for the VL
.iso image file, you can proceed to burn it as an image (see notes
above re proper burning).

After that, you may want to burn the ISO image onto a CD (but you
don't have to - see below). For this, use the program that is
provided by your CD-writer (e.g: Adaptech CD Writer, or Nero
Burner). Ensure you burn as an IMAGE not as a file.

Checksum Tools for Linux
------------------------

Most Linux systems already have the tools so you will not have to
download any utilities. Here is how to do that on Linux
console/terminal in three steps:


-  Downloading the ISO image and MD5:
   nohup wget
   ftp://anymirror/path-to/veclinux-6.0/iso-release/vl-6.0.???.iso
   nohup wget
   ftp://anymirror/path-to/veclinux-6.0/iso-release/vl-6.0.???.iso.md5.txt
   (\*\* Replace the pathname ??? above with correct path \*\*)

-  Checking the integrity:
   md5sum -c vl-6.0.???.iso.md5.txt

-  Burning the ISO onto a CD:
   cdrecord -v fs=6m speed=4 dev=2,0 vl-6.0.???.iso


Instead of the command line you can of course use "k3b" for the
above jobs.


Installing VectorLinux
======================

To obtain VectorLinux you need to either purchase a Deluxe CD from
our `CD Store <http://vectorlinux.com/cd-store>`_ or download an
"iso" image from a
`VectorLinux mirror site <http://vectorlinux.com/downloads>`_. Once
connected to the mirror site navigate to the
...distr/vectorlinux/veclinux-6.0 directory at the mirror site you
have chosen. Within that directory you will see two subdirectories:
/iso-release and /iso-soho (amongst others). The /iso-release
directory is where you will find iso images for the VL 6.0 Standard
and VL 6.0 Standard Live Editions. The /iso-soho directory is where
you will find iso images for VL 6.0 SOHO Edition (when available).
Then download your iso of choice.

Prior to starting the installation, you must know two things: 1)
what type of hard drive controller your computer has and 2) which
method you will use to install. Today, there are three common hard
drive controllers: IDE, SCSI, SATA and Adaptec(for CDROMS). You
must boot the installation using the kernel that supports your
controller. The default kernel works for SATA-IDE drives (which
probably includes about 95% of today's desktop/laptop computers).
The second concern is which install method to use. This depends on
the capability of the target computer. VectorLinux can be installed
via one of these methods:


-  Direct ISO File from a Windows host.
-  Direct ISO File from a Linux host.
-  Bootable CD.
-  from a USB device.
-  from a network.
-  Floppy disk and CD.

Supported controllers
---------------------

VectorLinux supports and can be installed on systems using
SATA/IDE/ATA hard drive controllers - the most popular ones in
laptop/desktop systems. Another well known standard is a SCSI
controller, but due to its high pricing, it is only common on
commercial server systems.

If you are having difficulties using your SATA drives, make sure
you have set the BIOS correctly. In a nutshell, set the BIOS to
"*Enhanced mode SATA only"*. This is counter intuitive, but it
means use enhanced mode only on the SATA, not just use the SATA and
turn off the PATA. If you set it to enhanced mode SATA+PATA, the
kernel will lock as it tries to use an IDE driver for the SATA
controller. Your symptoms will be that the kernel install may hang
after detecting hda - hdd.

Please know your hard drive controller type because it determines
the kernel required for installation. The default kernel supports
the SATA/IDE controllers (this is the one for probably 99% of
workstation computers on the market). Otherwise, you need to
specify either "scsi" or "adaptec" kernel during the install.

Direct ISO on a Windows Host
----------------------------

This is a new method that will save you having to burn a CD. Assume
that you already have Windows running on the computer, and the
partitions have been prepared as suggested above.


-  Download your iso of choice as described above. Move it to the
   top level directory (C:\\ or D:\\). Don't forget to check its
   integrity as explained previously.
-  You MUST now rename the iso to follow DOS 8.3 filename specs
   (e.g. VL60.ISO, not vl60.1.24vl6.iso), and the filename should be
   all capital letters.
-  The next files should be placed into C:\\loadlin
   
   -  **loadlin.exe**
      Get it from “install/loadlin/” directory within the FTP site.
   -  **initrd.img**
      Find it as “isolinux/initrd.img” within the FTP site

   
   -  **The kernel file that matches your system** (ide, scsi, sata,
      adaptec)
      You may select one of them from “isolinux/kernel/” directory within
      FTP site.


Then proceed with the installation:


-  Shutdown Windows to DOS mode.
-  Type “cd C:\\loadlin” <enter> (without quotes)
-  Type “loadlin ide root=/dev/ram rw initrd=initrd.img” <enter>
   (without quotes).

You should replace the “ide” with the name of the kernel that you
downloaded earlier. The standard installation process will be
started.

Direct ISO on a Linux Host
--------------------------

If you have another Linux running on the computer, this method will
be easy. You need to download the following files into a directory
(e.g: /home/download):


-  The chosen VectorLinux ISO image file.
-  **vlinstall-iso**
   Find it as “install/vinstall/vinstall-iso” within the FTP site

Now go to the text console (press Cntrl-Alt-F1) and login as root.
Proceed as follows:

-  Switch to run level 2 (or 3 in Slackware/Redhat, etc)
   *init 2 <enter>*
-  Go to the directory where you downloaded the files, i.e.:
   /home/download
   *cd /home/download<enter>*
-  Now, install using the ISO file
   *./vinstall-iso vl-6.0.iso <enter>*

Bootable CD
-----------

Use this method if you already have the VL install CD, and your
computer is capable of booting it.

First, you may need to change the boot order. Changing the boot
order allows your system to boot from different devices like your
hard drive, floppy drive or CDROM drive.
When you select an installation method, you might have to set up
your system to boot accordingly. To do this, immediately after your
computer starts up go to the BIOS options setup screen **(1)**.
There should be an option for boot order (general options are C
drive, A drive, CDROM drive, etc). Choose which should boot first
(in this case CDROM drive), save out of the BIOS screens and
restart the computer. You can change the boot order back when you
are finished installing VL.

.. note:: Not all systems use the "Delete" key to enter the BIOS.
    Some systems use one of the function keys (F1-F12). Some use a
    combination of keystrokes. Often the initial boot screen when you
    turn on your computer will tell you which keystroke to use to enter
    the BIOS setup. If not, check your computer or motherboard manual.
    Failing that check your computer manufacturer's website
    documentation for your specific computer model #.

After the boot order is properly set, place the VL installation CD
in the CD drive and reboot the system. The boot process will give
you a prompt. This is the chance for you to select the correct
kernel that supports your controller. If you do NOT have a SCSI or
Adaptec controller (99% of machines don't), then simply press
[enter] to use the default kernel (sata/ide) with the graphical
installer. Otherwise, type the kernel you want, e.g:

*boot: scsi [enter]*

If you wish to use the text based install procedure (recommended if
you have installed VL before, are not a newbie and/or prefer a
slightly faster install process) then type "linux" at the first
line install prompt.

That's it. The chosen graphical install procedure will then be
started.

USB Stick Install
-----------------

Two methods for installing VL from a USB stick (pen drive) are
discussed
`on the VL Forum <http://forum.vectorlinux.com/index.php?topic=5721.0>`_.
These methods have not yet been confirmed by other users but do
apparently work.

Network Install
---------------

You may install VL from a network as long as your computer is
capable of booting from a network (a PXE boot). This is useful for
some laptops that don't have an optical drive and are not capable
of booting from a USB device. The method is explained
`on our HOWTO Forum <http://forum.vectorlinux.com/index.php?topic=8038.0>`_.

Floppy Install
--------------

This method allows you to install VectorLinux using the VL CD (any
edition) and two floppy disks. You may have to use this method if
the target computer cannot boot from the CD (especially on older
laptops). We assume that you already have the VectorLinux CD
prepared and ready. You may use it to create the floppies on
another Windows or a Linux host. Preparation on a Windows HOST is
as follow:


-  Insert the CD into the drive, assumed to be drive D:
-  Launch a dos prompt
-  Insert blank floppy #1, enter this command:

   ::

       cd D:\install\rawrite
       rawrite
       Enter source file name: D:\install\floppy\bare.i
       Enter destination drive: A                   

   The bare.i is an kernel image file that supports IDE controller. If
   you have SCSI controller, use the scsi.s instead.
-  Take out floppy #1, Insert floppy #2, enter this command:
   
   ::

       rawrite
       Enter source file name: D:\install\floppy\rootdisk.img
       Enter destination drive: A


On a Linux host, the preparation is as follows:

-  Launch a terminal
-  Insert the CD and mount it.

   ::

       mount /dev/cdrom /mnt/cdrom

-  Insert blank floppy #1, write the kernel image into it using
   this command:

   ::

       cat /mnt/cdrom/install/floppy/bare.i > /dev/fd0

-  Take out floppy #1, Insert floopy #2, enter this command:
   
   ::

       cat /mnt/cdrom/install/floppy/rootdisk.img > /dev/fd0


Now with the VectorLinux CD and the two floppies:

-  Set the computer to boot from the floppy drive
-  Insert the CD
-  Insert the floppy #1
-  Boot the computer
-  On the boot: prompt, press [enter]
-  After asked, replace floppy #1 with floppy #2
-  VL install screen will appear


5a. The Graphical (GUI Based) Installation Process: Step by Step
================================================================

When you launch the installation process, the first screen
presented to you lets you choose either the default kernel
(IDE/SATA) or another kernel (by pressing F1). Normally you just
hit the enter key unless you have a SCSI or ADAPTEC hard drive.
After a few seconds the graphical installer will launch. For the
most part the graphical installer is intuitive and
self-explanatory. However, the following sections will touch on
some of perhaps the more confusing issues that may be faced by
newcomers to VectorLinux.

The first GUI installer screen is seen below. For all of the
installer screens to following applies:


-  the leftmost rectangular panel simply shows the progress of the
   install, with a red dot indicating the current process
-  the right panel is the area where the user may need to make
   input choices
-  the bottom buttons will always be: "Back" to go back to the
   previous install process, "Exit Installation" to immediately exit
   the Install (however, if you have made changes to partitions these
   will already be made and there is no backtracking from that
   process) and "Next" to go to the next Install process after you
   have made choices in the rightmost panel.

In this first process you choose the language for both the install
as well as for the Linux operating system itself:

|image1|

The second GUI installer screen - "Find Installation Media" is seen
below. This screen enables the user to choose a particular VL ISO
that may be located on either one of your hard disk partitions, a
USB device or an optical device. In the vast majority of cases it
will find the VL CD you burned to install and the screen is really
just for information purposes. However, if you have a number of VL
ISO files on different devices then you click on the upper
information widget to choose which ISO you wish to install.
Information is also provided to you on absolute minimum hard drive
space install requirements. Then click Next.

|image2|

Now comes the disk partitioning process. If you are a newbie and
have not already done so please ensure that you have read section 2
above concerning partitioning. At this point you are given the
option to use existing disk partitions which you may have already
created with other partitioning software, or to modify (and create)
partitions from within the installer. The latter option is the
easiest method.

|image3|

If you chose the option to "Modify" above you are then presented
with the "gparted" application to either delete, resize, create,
format new or existing partitions. gparted documentation is
available
`at Sourceforge <http://gparted.sourceforge.net/documentation.php>`_ 
so we will not reiterate that information here.

|image4|

The next step is to choose the partitions for the VL install and
how to use them. Click the upper right Help button for more info. A
complete list of partitions found on your hard drives is presented
to you. We suggest you reread Section 2.1 above concerning
recommended VL partitions and then proceed by selecting the "mount
point" (ie: "root" / , "swap" swap and "home" /home). You should
then choose a filesystem (either ext3 or reiserfs are recommended
for normal home use).

|image5|

On the next two screens the user selects whether to do a full
install (recommended) or custom install. The custom install is
really only necessary where the user has hard drive space
limitations. The difference between a frugal and full install is
really only about 1.5GB of space. By clicking on the left side of
the package name the user can choose whether or not to install a
specific package. Obviously, this should only be done if the user
knows what the package is actually used for!

|image6|

|image7|

The next screen is an installation summary. Ensure your chosen
partitions are correct as are the packages you want to omit,
because the next stage is the actual installation to the hard drive
of all the system files and packages.

|image8|

Progress of VectorLinux operating system files and packages
installation is then shown with accurate status bars. On modern
computers this process should take less than 15 minutes.

|image9|

Once the installation of system files is complete the system
configuration steps are started - the first of these being to
install the
`Linux Loader - "lilo" boot manager <http://en.wikipedia.org/wiki/LILO_(boot_loader)>`_.
For newbies we suggest that you go with the default values as
presented to you and click the "Next" button. Lilo will find any
other operating systems on your hard drives (such as Windows if
this is a dual boot installation) and will create and entry for all
operating systems in the boot loader. For more advanced users this
panel enables you to:


-  not /usr/share/vim/vim71/compiler/tidy.viminstall lilo (for
   instance if VL is a second distro and you already have grub)
-  install the boot loader to somewhere other than the MBR (eg: a
   floppy disk or USB device)
-  choose whether or not to include a specific operating system on
   the boot menu
-  choose the name for the OS as it will appear on the boot menu
-  choose kernel boot options for each specific OS
-  choose the video resolution for the boot loader; useful where
   you might have an older 15" monitor

|image10|

We now select our timezone.

|image11|

The installer then prompts for a root password. If you want proper
Unix based security you must enter a hard root password on this
screen.

|image12|

You may now enter as many users as you wish. You should always have
at least one user account which you will use as your normal account
- normally you would just use your first name in lowercase letters
for this account. Never use the root account for normal work. To do
so violates the basic strong security for which Unix/Linux is
famous. In the "Rights Management" panel we suggest you leave the
default values unless you are an experienced Linux user and have
specific requirements for access rights for a particular user. once
you have entered user details you *must* click then click the
"Create User" button. You may create a number of users on this same
screen.

|image13|

On the Network Configuration screen you must give your computer
both a name and domain name. It is recommended that for normal home
network configurations you keep the defaults as prsented, i.e. use
WiCD to manage the networks, use DHCP to domain name service, etc.
If you have a wireless device and a regular ethernet device they
will show as tabs: "eth0" and "ath0" or "wlan0". You can configure
each network device separately, but for newbies the defaults will
probably suffice. Manual DNS servers specifications are only for
networks where you have been given a specific IP address. Most
Internet Service Providers (ISP) use DHCP.

|image14|

The Final Hardware Configuration screen requires no input. It
simply notifies you that the VL installation is now complete and
you may reboot the computer. Upon reboot there will still be some
hardware configuration setup requiring input from the user. For
that information please jump to Section 5c.2 below.

|image15|


5b. The Text-Based Installation Process: Step by Step
=====================================================

When you launch the installation process, the first screen
presented to you lets you choose either the default kernel
(IDE/SATA) or another kernel (by pressing F1).

|image16|


#. Start up – On the next screen you have four options:
   
   -  Select the keymap to be used during the installation. You should
      select this menu for the first time if your keyboard in a non-US
      layout.
   -  Start the installation. The reason why we are here!
   -  Repair lilo (Linux Loader). This is a handy utility in case you
      have an installed Vector Linux, but somehow you cannot boot it
      because of a corrupted LILO.
   -  Exit. This will bring you to the Linux command prompt. It might
      be required if something is wrong with the installation and you are
      capable enough at the Linux command line to fix it manually.

   |image17|

#. Once you start the installation, the routine will look for the
   installation media in the following places:
   
   -  Any hard drive partition that contains installation files
   -  Any hard drive partition that contains the ISO file in the top
      directory
   -  CDROM drive

   If a media is found, it will ask you to confirm. Select Yes to
   proceed, No to search another media.
   |image18|

#. The next screen shows you the hard drive requirements of the
   soon to be installed edition. If you have already set up your
   partitions, then start the installation now. Otherwise you can
   create or modify the hard drive partitions using the built in
   tools. RESIZE menu is a simple front end for GNUParted. Meanwhile
   the FDISK menu will launch a menu-driven partitioning program
   called cfdisk (see the three images following). Warning: do not
   proceed to the INSTALL menu if you don't have the required 3
   partitions (root, swap and home) at the required sizes, because the
   install will surely fail.
   |image19|

   |image20|

   |image21|

   |image22|

#. You are now presented with a list of the next steps the routine
   will perform, it goes through each of these steps in order.
   |image23|

#. In the check-files screen, you may choose which files are to be
   verified for any damage or corruption. If you are confident enough
   about them you could skip the verification, but if the files are
   somehow damaged the installation will probably fail later. If you
   have already done an *md5sum* check on the .iso file then this step
   is probably unnecessary.
#. Now the routine will present you with any swap partition
   detected. Choose the swap partition to use and select OK..
#. The next step is to choose your root partition. This is the
   partition on which you want to install VectorLinux. You will have
   the chance to select a different partition for your /home directory
   later.
#. Then select the filesystem you prefer for the root partition:
   reiserfs, ext3 or ext2.
   ext2 is the older Linux filesystem, reiserfs and ext3 are both
   newer and more robust journaling filesystems. ext3 is a general
   standard and reiserfs is particularly fast with many small files.
#. Next, you can choose to use a different partition for your /home
   directory or use the same one that is used as root. The "/home" is
   where files that belong to users are to be stored, and "root" is
   where the Operating System´s files and all packaged software will
   be stored.

   |image24|

   |image25|

   |image26|

#. The next two dialog screens allow you to choose what main
   packages and individual software applications you wish to install.
   |image27|

   |image28|

#. You are now ready to install VectorLinux to your system. You can
   review the choices you have made and go back and do some
   modifications, or you can select OK to continue installing
   VectorLinux. Once you hit OK, and the installation proceeds, it
   will format the partitions you selected for root and /home, and any
   information in them will be erased, so please verify everything
   before continuing.

   |image29|


The installation will take a little while, so you may have a short
break. The installation time will vary depending on your system´s
speed. It can be as quick as 10 minutes. After some minutes, your
monitor screen may go blank as it enters a power saving mode. If it
does, press **[SHIFT]** on the keyboard to restore your monitor. DO
NOT hit [enter] or [space] as you could inadvertently accept a
choice the install routine might have reached at that point.
After the installation is done, you will be required to do some
basic configuration, as explained in Section 6.


5c. Post-Install Configuration
==============================

Once VL has been installed, a configuration screen is presented.
This allows you to configure sound, video, network, etc. This
screen can also be accessed later using "Vasm" as root.

However, it is recommended that you **do not skip** this
configuration step at installation time.

5c.1 Set Up LILO
----------------

You will be prompted to set-up “LILO”, the Linux Loader that boots
the system.

Choose where to install lilo:

- the root partition (if you installed VL to /dev/hda2, this would
  install lilo to /dev/hda2)
- the MBR (master boot record) for the disk
- a floppy drive

|image30|

If you want Lilo to take over the boot process entirely, install to
the MBR of the hard disk (this is the most common scenario). LILO
will detect other operating systems on your hard drives and make an
entry for them in its boot menu. If you already have another boot
manager installed on some partition that can point to your Linux
partition, then you can install Lilo to the root partition where
you installed VL. If you are not sure and you don't want to cause
any problems, install to a floppy. But you must then ensure that
your computer's BIOS is set to boot from the floppy drive first.
This is the slowest boot method, and only suggested if you do not
want to alter your existing boot manager. For most systems
installing to the MBR is safe and easy.

|image31|

Choose to enable framebuffer or not. This option affects how the
boot process looks, and the onscreen indications will let you know
what option is best for you.

|image32|

|image33|

Add any additional parameter you need to boot you system. If your
system didn´t require any special kernel parameters to perform the
installation, then leave this field empty.

If you have them, Lilo will detect the partitions and Operating
Systems on your hard disk and allow you to select which ones you
want to have available in your new boot menu.

After you have installed VL, you can change the lilo configuration
by using "vasm" as root, which will bring up the configuration
screen again, or, also as root, you can edit the
/etc/lilo.conf file by hand. After editing the file, be sure to
issue the following command as root: "/sbin/lilo -v". This will
commit the changes to the lilo bootloader.

5c.2 Configuring VectorLinux
----------------------------

Once Lilo is installed, the next steps are to configure this new
system. If you installed with an ISO file directly from a Linux
host the system will have to reboot first before doing this next
configuration step.

|image34|

The first screen shows the configuration steps, and allows you to
choose which ones are to be performed. The wise decision is to
select them all, unless you have experienced problems with a
particular step previously. At this point you are walked through a
series of screens to configure the keyboard, auto-setup the basic
hardware, select network settings, set the video, sound, and time
zone. The configuration will try to detect most of the settings
automatically.

|image35|

|image36|

|image37|

|image38|

|image39|

|image40|

|image41|

|image42|

|image43|

|image44|

|image45|

|image46|

|image47|

In the last step, the configuration asks you to change the root
(superuser, administrator) password and add the first ordinary
user. Please don't skip this step for your own security. On any
operating system, working daily as root is not advisable, so you
should set up a normal user and use that for normal tasks and
resort to root only for administering the system.

When this is done, you simply press “OK” to restart the system and
boot into VectorLinux for the first time.


Using the system for the first time
===================================

Login to VectorLinux
--------------------

If you chose to boot into RunLevel 4 (the X GUI system), when the
VectorLinux boot process has been completed you will be presented
with a graphical login screen where you should enter your personal
username and password (do not use root unless absolutely
necessary). After logging in the desktop environment will load and
within seconds you will see the desktop. Have fun!

As well as the GUI desktop, you will get six text consoles. You may
switch between them by pressing [Ctrl]-[Alt]-[F1] through
[Ctrl]-[Alt]-[F6] on the keyboard. You will be asked for a username
and password. If you did not change the root´s password during
installation you should use root as username and leave the password
empty. Otherwise, use the name and password of the account you
created earlier. You may return to the GUI by pressing
[Ctrl]-[Alt]-[F7]

If you chose to boot into RunLevel 2 (a text console environment)
you may switch to RunLevel 4 (GUI) by typing:

::

        user:$ startx
       

Alternatively, you can launch the GUI login mode by switching
runlevel into 4 or 5. Please login as root on the console, then
type:

::

        root:# init 4
       

System Administration
---------------------

If you want to further configure your VL system, you should launch
`VASM (Vector Administration and System Menu) <vl5_admin_vasm_en.html>`_
from either a terminal or from the desktop menu. You will be asked
for root's password. This utility is explained in detail in the
previous link and will not be reiterated here.

Reboot and Shutdown
-------------------

There are many ways to reboot the system, including

- From a console (not GUI) press Ctrl-Alt-Del
- From a console or GUI terminal, type "reboot"
- From a GUI windows manager, choose "reboot" menu.

Meanwhile, to turn off the system, you may:

- Type "halt" or "poweroff" from a console.
- Select "shutdown" menu from a windows manager if available.


Troubleshooting
===============

**Error type 1**

*installation gives you a message saying that it can't find the vector bz2 kernel and / or saying that /dev/xxxx is not a valid block device.*

This error usually appears when you have more than one optical
drive (CD/CD-RW/DVD/DVD-W) and you are trying to install Vector
from the second unit. Move the installation CD to the first drive.

**Error type 2:**

*Installation halts or does not start properly after a seemingly correct installation or you get messages about CRC errors during install*

That kind of issue often suggests file corruption during download
or a faulty burning process.

The first thing to do, if you haven't already done so, is to check
the .iso file for corruption; please refer to section 3 of this
manual. If the .iso file passed the md5sum integrity check , then
you could try burning the file at a slower speed or use higher
quality media.

Remember to burn as a cd image, not as a conventional file!

**Error type 3:**

You get one of these two messages:

*Kernel Panic: Aiee, killing interrupt handler!In interrupt handler - not syncing.*

or

*Error! There was a problem!Code: 39 36 75 03 5b 5e c3 5b 89 f0 31 c9 ba 03 00 00 00 5e e9 cbInstallation not completePlease press enter to activate this console*

Those errors are usually related to old hardware, and could mean
that you need to pass some kernel commands to the boot process. It
could also mean that there is some sort of hardware problem with
your computer, but if you had it working previously with another
operating system then it is probably one of the following:

Some commands you may try are:

*linux mem=16M* (replace 16 with the correct amount of memory in
your PC)
*linux ide=nodma* (disable udma access, for old hard drives)

These commands disable power management, Notebooks often require
them:

*linux noacpi*
l*inux noapmlinux pci=noacpilinux acpi=offlinux apm=off*

**Error type 4:**

*Your SCSI or SATA hard drive is not available to install VL on it.*

The required driver for your SCSI card is not being loaded, and
therefore your disks are not seen by the install routine.

**Error type 5****:**

Installing from CDROM drive fails with this error:
"*mount: /dev/scd7 is not a valid block device*"

Your CDROM or CDRW drive requires scsi emulation. When the
installation greets you with the first prompt (where it says
"boot:" at the bottom left) you should type :

*ide hdx=ide-scsi*

(where x is your cd-rom unit).

If that doesn't work restart and try writing

*scsi hdx=ide-scsi*

(where x is your cd-rom unit).

.. note:: The Linux Kernel 2.6.x treats CD-Rx drives in a different way than 
    previous kernels, so this problem shouldn´t arise, it is still mentioned
    here for the record.*

**Error type 6:**

The installation halts somewhere near the end during the install of
packages and the install process gives you an error message that it
cannot proceed. This often indicates that you may not have
apportioned sufficient hard drive space for the "/" (root)
partition. VectorLinux requires a minimum of 3GB to install, 3.8GB
for the SOHO edition. You will need to increase the size of the
root partition before proceeding to re-install the system. This can
be done using cfdisk during the second install screen.

**Error type 7:**

The installation locks up, with no response from the keyboard, and
often occurs right near the beginning of the install process when
you see the kernel-loading messages. This usually indicates that
you have faulty RAM. Use a Live Linux CD and run "memtest" to
confirm that you have a faulty RAM memory module on your computer.


.. |image1| image:: ../_static/images/img_install/gui1.png
.. |image2| image:: ../_static/images/img_install/gui2.png
.. |image3| image:: ../_static/images/img_install/gui3.png
.. |image4| image:: ../_static/images/img_install/gui4.png
.. |image5| image:: ../_static/images/img_install/gui5.png
.. |image6| image:: ../_static/images/img_install/gui6.png
.. |image7| image:: ../_static/images/img_install/gui7.png
.. |image8| image:: ../_static/images/img_install/gui8.png
.. |image9| image:: ../_static/images/img_install/gui9.png
.. |image10| image:: ../_static/images/img_install/gui10.png
.. |image11| image:: ../_static/images/img_install/gui11.png
.. |image12| image:: ../_static/images/img_install/gui12.png
.. |image13| image:: ../_static/images/img_install/gui13.png
.. |image14| image:: ../_static/images/img_install/gui14.png
.. |image15| image:: ../_static/images/img_install/gui15.png
.. |image16| image:: ../_static/images/img_install/inst1.png
.. |image17| image:: ../_static/images/img_install/inst2.png
.. |image18| image:: ../_static/images/img_install/inst3.png
.. |image19| image:: ../_static/images/img_install/inst4.png
.. |image20| image:: ../_static/images/img_install/inst5.png
.. |image21| image:: ../_static/images/img_install/inst6.png
.. |image22| image:: ../_static/images/img_install/inst7.png
.. |image23| image:: ../_static/images/img_install/inst8.png
.. |image24| image:: ../_static/images/img_install/inst9.png
.. |image25| image:: ../_static/images/img_install/inst10.png
.. |image26| image:: ../_static/images/img_install/inst11.png
.. |image27| image:: ../_static/images/img_install/inst12.png
.. |image28| image:: ../_static/images/img_install/inst13.png
.. |image29| image:: ../_static/images/img_install/inst14.png
.. |image30| image:: ../_static/images/img_install/inst15.png
.. |image31| image:: ../_static/images/img_install/inst16.png
.. |image32| image:: ../_static/images/img_install/inst17.png
.. |image33| image:: ../_static/images/img_install/inst18.png
.. |image34| image:: ../_static/images/img_install/inst19.png
.. |image35| image:: ../_static/images/img_install/inst20.png
.. |image36| image:: ../_static/images/img_install/inst21.png
.. |image37| image:: ../_static/images/img_install/inst22.png
.. |image38| image:: ../_static/images/img_install/inst23.png
.. |image39| image:: ../_static/images/img_install/inst24.png
.. |image40| image:: ../_static/images/img_install/inst25.png
.. |image41| image:: ../_static/images/img_install/inst26.png
.. |image42| image:: ../_static/images/img_install/inst27.png
.. |image43| image:: ../_static/images/img_install/inst28.png
.. |image44| image:: ../_static/images/img_install/inst29.png
.. |image45| image:: ../_static/images/img_install/inst30.png
.. |image46| image:: ../_static/images/img_install/inst31.png
.. |image47| image:: ../_static/images/img_install/inst32.png
