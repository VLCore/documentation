
##############
Printing guide
##############


Pre-Installation Check
======================

Some printer manufacturers have made more efforts to support Linux
than others. This support takes the form of either releasing
Linux-specific drivers or at least releasing crucial hardware data
to the Linux community so others can write the necessary drivers.
Two manufacturers who have excelled are Hewlett Packard and
Brother. Others are improving their Linux support. To date probably
the least Linux-friendly manufacturer has been Lexmark. Before
purchasing or enabling a printer please check that it is supported
by Linux at
`the "OpenPrinting" website <http://openprinting.org/printer_list.cgi>`_.
"Most" printers now manufactured do work with Linux but there are
some exceptions - the website above is definitive.

If you have a parallel port printer you should have enabled the
parallel port during the install process. If you did not do this go
to VASM, SUPER, HARDWARE, HWSET, enable parallel. If you have a
newer HP printer you should check that either the hpijs and/or
hplip packages have been installed to your system - if not then
install them (use "gslapt"). If you have a printer which requires a
special driver you must install this driver prior to launching the
CUPS configuration steps (below). The website above will point you
in the right direction. Please ensure you follow the driver install
directions exactly or it is unlikely you will magically get your
printer to work.

If you are going to use a printer on a Windows server you should
check if the printer is available (see the section "Printing to
Windows Servers via Samba").


Install & Configure your printer using CUPS
===========================================

Open your web browser of choice and in its address bar type:

**``http://localhost:631/``**
Choose "Do Administration Tasks" and then enter "root" as username
and root´s password at the prompt.

To add a printer, select "Add Printer" and answer questions as
required. If your printer is a USB printer you should see the USB
device and the printer name in the drop down devices list. For a
parallel port printer you normally select "#LPT1" from the device
list.
When this is complete you should test the printer. (Select Printers
and then select Print Test Page). You may open your browser and
enter the CUPS server page (as described above) whenever needed to
administer your printer(s). The
`CUPS help pages <http://localhost:631/help/>`_ also contain very
useful documentation. If you are going to use a printer on a
Windows server (or a Linux Server which supports SAMBA) see the
section "Printing to Windows Servers via SAMBA" below.


Printing to Windows Servers via SAMBA
=====================================

(Adapted From CUPS Software Admin Manual)

One way to print to a printer on a Windows Server is through the
Microsoft Server Message Block ("SMB") protocol. Support for this
protocol is provided with the free SAMBA software package. This is
included in the base install of VectorLinux v6.

If you are going to use a printer on a Windows server ensure that
you can access the server with the printer and determine share name
via smbclient:

``/usr/bin/smbclient -L server -U user``
where server is name of the server and user is a login name on the
server.

You should get something like:

::
    added interface ip=192.168.0.4 bcast=192.168.0.255
    nmask=255.255.255.0
    Got a positive name query response from 192.168.0.4 ( 192.168.0.4 )
    Password: (not shown)
    Sharename Type Comment
    PRINTER$ Disk
    INKJET Printer
    DOWNLOAD Disk
    IPC$ IPC Remote Inter Process Communication
    In this case the sharename is INKJET.

Before you proceed with the printer installation, check to see if
there exists a symbolic link *smb* in the directory
*/usr/lib/cups/backend/.* (If you installed CUPS using the
preceding instructions the link should exist). If it is not there,
you will need to create it by running the following command:

``ln -s `which smbspool` /usr/lib/cups/backend/smb``
``ln -s /usr/bin/smbspool /usr/lib/cups/backend/smb``

Notice that "which smbspool" is enclosed in back ticks, not single
quotes! The back ticks will execute the command within and use the
result in the ln command. The latter command works for the default
installation of all versions of VectorLinux 6.0. Alternatively, you
can create this link using Midnight Commander.

Once you have made the link you can configure your printer as in
section 3. When prompted for device, choose "Windows Printer via
SAMBA" which is near the bottom of the list of devices. When
prompted for device URI set it with:

``smb://user:pass@workgroup/server/sharename``

The *workgroup* name need only be specified if your system is using
a different workgroup. The *user:pass* strings are required when
printing to Windows NT, 2K or XP servers or to shares with
passwords enabled under Windows 95 and 98.

In these cases the entry could be simplified to one of:

``smb://workgroup/server/sharename<br> smb://server/sharename<br> smb://user:pass@server/sharename``

.. note:: 
    The user/pass must match an existing, active account on
    the server.


Sharing Your Printer with Windows via SAMBA
===========================================

(Adapted from Debian and Windows Shared Printing mini-HOWTO by Ian
Ward
`http://www.tldp.org/HOWTO/Debian-and-Windows-Shared-Printing/) 
<http://www.tldp.org/HOWTO/Debian-and-Windows-Shared-Printing/>`_).

In this section we discuss how to set up VectorLinux to act as a
print server for other printers on our network via SAMBA. It is
assumed that you have your printer working locally using CUPS. We
will need to make several changes to both CUPS and SAMBA
configuration files.

Each of the Windows clients must have the appropriate printer
drivers.

Setting up SAMBA
----------------

If you are allowing anonymous access to your printer you will need
to create a user account for remote print jobs. As root (or su) do
the following:

``useradd -d /home/smbprint -s -m /bin/false smbprint``
This command adds a user called "smbprint" to your system. Make
sure there is enough disk space in */home/smbprint*, the "smbprint"
user's home directory, to spool files. Since we have no password
for the account "smbprint" we include "-s /bin/false". This causes
an error if someone tries to access the system using this account.
If you have configured CUPS to restrict printing to certain users
on your system, you must allow the "smbprint" user to access
printers you want to share.

The Samba configuration file is */etc/samba/smb.conf*. The
following is an example configuration file set up to use CUPS with
the "smbprint" user:

::
    [global]
    workgroup = WORKGROUP
    printcap name = cups
    printing = cups
    security = share
    guest account = smbprint
    
    [printers]
    comment = all printers
    printable = yes
    guest ok = yes
    create mode = 0700
    guest only = yes
    use client driver = yes
    path = /home/smbprint
    browseable = yes

The last line allows the Windows (and other SMB) clients to see the
share when browsing.

Please note that this configuration will allow printing by anyone
that can make a network connection to your computer and is not
recommended for computers on untrusted networks, such as computers
with direct Internet connections. If you need to implement access
control, set security = user or security = domain and read the
Samba man pages for further information. In addition you can refer
to "The Unofficial Samba HOWTO" at
`http://hr.uoregon.edu/davidrl/samba/ <http://hr.uoregon.edu/davidrl/samba/>`_
for further pointers.

Once you have added the above settings to your Samba configuration
file you must restart Samba with the command:

``/etc/rc.d/init.d/samba restart``

.. note::
    The above assumes that script */etc/rc.d/init.d/samba* exists and
    is executable. A copy of the script is available in the directory
    */etc/rc.d/init.d/sample-scripts* if you have set up SAMBA to start
    as part of RC.M.

If SAMBA does not start at boot, one method to do this is to create
symbolic links, S44samba and K55samba, to the above script in
*/etc/rc.d/rc2.d* if you boot to text mode and */etc/rc.d/rc5.d* if
you boot to a GUI.


CUPS Configuration
------------------

Windows printer drivers format their output for the printer before
sending it across the network. You must configure CUPS to accept
the pre-formatted output by uncommenting the following line from
*/etc/cups/mime.convs*:

::
    *application/octet-stream application/vnd.cups-raw 0 -*

Also uncomment the following line from */etc/cups/mime.types*:

::
    *application/octet-stream*

Now CUPS must be told to allow connections from other machines on
the network. Add these lines to */etc/cups/cupsd.conf*:

::
    <Location /printers>
        AuthType None
        Order Deny,Allow
        Deny From None
        Allow From All
    </Location>

As in the Samba configuration, this configuration allows any
computer to connect to your printers and is not recommended for
computers on untrusted networks. For example, if you want restrict
printing to the subnet 192.168.0.\* replace *Allow From All* with
*Allow From 192.168.0.0/24*.

For information about tightening access control to your printers,
see the cupsd.conf man page and the CUPS documentation.

Finally, restart cups with the following command:

``/etc/rc.d/init.d/cups restart``

Your Linux printers should now be accessible to Windows PCs on the
LAN. Follow the usual steps for adding a network printer to your
Windows PCs, and remember to print a test page.

