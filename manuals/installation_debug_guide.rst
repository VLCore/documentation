#############################
Installation Debugging Guide
#############################

If you have encountered a problem during installation, follow this
guide to collect all the necessary information and report the issue
to the development team.

.. note::
  This guide applies to |vector_edition| Versions 7.1 and newer only.


Collecting the logs
===================

The installation logs will help determine what went wrong during the install
process.  Here is how to collect the logs.

You will need to save this data to an alternate location where you can find it
later, or paste to a an online pastebin for later retreival.  In this guide
we will assume that you have a partition ``/dev/sdb1`` formatted and ready
to accept data.

From the GUI installer
----------------------

If you installed using our GUI installer and your installation has stalled or
has exited with an error:

- Access the application menu by moving the mouse pointer to the **bottom left**
  corner of the screen.  Once the taskbar and menu become visible, click
  ``Menu -> Terminal``.  This will open a terminal window
- Mount your partition where the logs will be saved.

  - ``mkdir /mnt/sdb1``
  - ``mount /dev/sdb1 /mnt/sdb1``
- Copy the log files to the destination partition

  - ``cp /root/installer.log /mnt/sdb1/installer.log``
  - ``cp /root/subprocess.log /mnt/sdb1/subprocess.log``


From the Text installer
------------------------

If you installed using our Text installer and your installation has stalled or
failed with an error, here is how you can retreive the logs.

- Access a new TTY by pressing ``CTRL+ALT+F3``.  F4 can be used just the same.
- Log in as user ``root``.  When it asks for password, just press ``Enter``
- When the installer shows up again, press the ``< Back > `` button.
- Mount your partition where the logs will be saved

  - ``mkdir /mnt/sdb1``
  - ``mount /dev/sdb1 /mnt/sdb1``
- Copy the log files to the destination partition.

  - ``cp /root/installer.log /mnt/sdb1/installer.log``
  - ``cp /root/subprocess.log /mnt/sdb1/subprocess.log``


Filing a the bug report
=======================

If your installation has failed, we ask that you file a bug report at
http://bitbucket.org/VLCore/ in the correct project (ie, vl71, vl72) that
corresponds to the version you have installed.  In your bug report you should 
include the log files you retreived from the installation.

.. note:: 
  If your machine has a network connection during installation (test by pinging
  google.com for example), you can paste your logs to an online pastebin right 
  from the terminal or shell.  For example, to paste to sprunge.us, you can
  ``cat /root/installer.log | curl -F 'sprunge=<-' http://sprunge.us``.

  If you use one of these services, make sure you paste the contents on the bug
  report itself, and not just a link.  Most of these services will remove the
  content of your paste after a few days.

