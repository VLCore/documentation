
#########################################################
|vector_edition| Desktop Environments and Window Managers
#########################################################


Introduction
============

Newcomers to Linux are often confused by the variety of various desktops
available to Linux. MS Windows and Apple provide only one. The
attraction of Linux to some, is its variety. There is a "look" for every
taste and a "usable desktop" for everything from very old 486 computers
to modern expensive gaming computers.

What probably confuses newbies the most is the fact that Linux makes a
distinction between a `"desktop environment"
(DE) <http://en.wikipedia.org/wiki/Desktop_environment>`_ and a `"window
manager" (WM) <http://en.wikipedia.org/wiki/Window_manager>`_. The
former tends to include GUI utilities to easily alter the look of the
desktop, automatically update the menu on installation of new
applications, provide small useful auxiliary applications and some
system configuration tools. The latter however, only creates the window
borders on applications and manages movements of windows on the screen.
Window Managers by themselves may not provide desktop icons, may not
auto update their menu and may not provide GUI tools to change the look
of the desktop. However, otherwise they are as configurable as desktop
environments but the user sometimes must edit their configuration files
by hand, rather than using a GUI tool.

We recommend that newbies stick to either the KDE DE when using SOHO or
Xfce when using Standard. Both these DE's provide facilities for
automatic menu updating, easy maintenance of desktop icons and easy to
use GUI utilities for changing the look of your desktop. The other
environments are less intuitive for the beginner.

To satisfy all tastes |vector_edition| provides a variety of DE's and WM's,
depending on the version of VL:

-  VL SOHO: `KDE (DE) <http://www.kde.org/>`_ and `Fluxbox
   (WM) <http://fluxbox.org/>`_.
-  VL Standard: `Xfce (DE) <http://www.Xfce.org/>`_ and `lxde (small
   DE) <http://lxde.org/>`_.
-  VL Light: `IceWM (WM) <http://icewm.org/>`_ and `JWM
   (WM) <http://joewing.net/programs/jwm/>`_.
-  VL Live: `Xfce (DE) <http://www.Xfce.org>`_ and `Fluxbox
   (WM) <http://fluxbox.org/>`_ and `JWM
   (WM) <http://joewing.net/programs/jwm/>`_.

Notice that each DE-WM link above points to the specific website for
each application where full documentation is available that would answer
any question you have. The sections below will summarize very briefly
the most important concerns newbies have raised on how to deal with each
DE-WM, specifically as they would apply to |vector_edition|. Where to find
the main configuration files and utilities is also listed below.


KDE Desktop Environment
=======================

KDE is supplied with the VL SOHO Edition. KDE and Gnome (not provided by
VL) are considered to be the two dominant, heavyweight and most
fully-featured desktop environments now in use on most Linux systems
around the world. The KDE desktop is relatively similar to that provided
by MS Windows Vista. It requires a reasonably modern computer with about
#12MB of RAM to run effectively. It can run with just 256MB of RAM but
when other large applications are opened your machine may begin to slow
considerably.

KDE's desktop and its icons can quickly be configured by using one right
mouse-button click when the cursor is on the desktop and using that
context menu. The KDE Panel can be configured likewise by right-clicking
on the panel. Other configuring of the system and its environment can be
accomplished by KDE Menu, Control Centre. The configuration options in
the control center can keep a hacker busy for months!


Fluxbox Window Manager
======================

Fluxbox is supplied as an alternative, lighter weight window manager
with the VL SOHO 5.9.1 Edition. At the login screen click the "Session"
button and choose Fluxbox. Right-clicking on a Fluxbox desktop raises
the Applications Menu. There are no desktop icons under Fluxbox but wbar
(an animated MacOSX-like launch bar) may be included. Right clicking the
Fluxbox panel gives you panel configuration options. In the Menu there
is also a "Fluxbox" section where you may alter some Fluxbox settings.
Right clicking the top bar on any application window enables complete
control over that window itself - often more control than provided by
any other DE or WM. Note that neither HAL nor vl-hot will produce
desktop icons or raise any windows to enable GUI configuration of any
newly attached hardware devices or drives. You will have to determine at
what mount point these devices have been mounted and then use the file
manager to navigate to them. Please read the `Mounting Devices
Guide <vl6_mounting_guide_en.html>`_. To configure other aspects of
Fluxbox or to edit the Menu you edit fluxbox configuration files which
are found in your ~/.fluxbox directory (where "~" represents your home
directory, e.g. /home/larry/.fluxbox). Fluxbox config files are:

-  ~/.fluxbox/apps : specifies specifics of how to display saved
   application window positions/sizes, etc.
-  ~/.fluxbox/init : initial fluxbox config file
-  ~/.fluxbox/menu : the fluxbox menu
-  ~/.fluxbox/keys : hotkey configurations
-  ~/.fluxbox/startup : specifies applications to launch on start


Xfce Desktop Environment
========================

Xfce is supplied with VL6.0 Standard and VL5.9 Live Standard as the
default desktop environment. Xfce is less resource intensive than either
KDE or Gnome, yet still provides all desktop utilities that most Windows
and Mac users would expect. Right-clicking on an Xfce desktop raises the
Applications Menu. Right clicking the Xfce panel give you panel config
options. Right-clicking on any desktop icon gives you options for adding
more icons, editing that icon and changing the desktop background. The
Xfce menu is updated when new, properly packaged applications are
installed. Both HAL and vl-hot work well with Xfce and appropriate icons
are created or appropriate applications are launched when you attach new
hardware to your machine (i.e. digital cameras, external drives, etc.).
Further tweaking of the Xfce desktop is done via its extensive "Settings
Menu".


LXDE lightweight Desktop Environment
====================================

LXDE is supplied with VL6.0 Standard. To use it you click the "Session"
tab on the login screen and choose LXDE rather than the default Xfce.
LXDE is faster and takes less system resources than Xfce, but as a
consequence is not as easily configurable by the novice user.
Right-clicking on an LXDE desktop raises the LXDE Settings Menu. Right
clicking the LXDE panel give you panel configuration options. The main
menu contains a "Settings" submenu where you can also configure the look
of LXDE's desktop. HAL works well with LXDE's file manager
(`PCManFM <http://pcmanfm.sourceforge.net/>`_) so that when you attach
new hardware to your machine (i.e. digital cameras, external drives,
etc.) an entry appears in the rightmost panel of the File Manager
(double click the "My Documents" icon). Further tweaking of the LXDE
desktop is done via editing files in the ~/.config/lxde directory.


IceWM Window Manager
====================

IceWM is an attractive, lightweight and fast Linux window manager and is
the default manager for VL Light. The developers of VL Light have spent
considerable efforts in making IceWM more newbie friendly. They have
included the `PCManFM desktop <http://pcmanfm.sourceforge.net/>`_ and
prepared scripts to auto update the menu when requested. HAL works well
with PCManFM and therefore attached USB devices will appear in the file
manager right hand panel when you double click on the "My Documents"
icon. Right-clicking anywhere on the desktop will enable you to control
icons and change desktop settings. If you add new applications you need
to update the menu by going to Menu, System, Update Menu. You may change
your IceWM desktop theme from the main menu. If you want to delve into
details of changing IceWM configuration files they are found in ~/.icewm
and the relevant files are: keys, menu, startup, theme, toolbar and
preferences. See the `IceWM documentation <http://www.icewm.org/>`_.


JWM Window Manager
==================

JWM is included with VL Light and VL Live. Of all the DE's-WM's used in
various |vector_edition| Editions the JWM Window Manager is probably the
fastest and uses the least system resources. It is best used on slower,
older computer hardware that has minimal RAM. Therefore, it also has the
fewest user friendly features. However, the developers of VL Light have
spent considerable efforts in making JWM more newbie friendly. They have
included the `PCManFM desktop <http://pcmanfm.sourceforge.net/>`_ and
prepared scripts to auto update the menu when requested. HAL works well
with PCManFM and therefore attached USB devices will appear in the file
manager right hand panel when you double click on the "My Documents"
icon. Right-clicking anywhere on the desktop will enable you to control
icons and change desktop settings. If you add new applications you need
to update the menu by going to Menu, System, Update Menu. To configure
anything to do with JWM you will need to edit your ~/.jwmrc file or the
files located in the JWM directory (~/.jwm). To do that you will need to
read the documentation at the `JWM
website <http://joewing.net/programs/jwm/>`_.

