
#####################################
The |vector_edition| Packaging System
#####################################


Introduction
============

Unlike the Microsoft Windows world which uses executable *.exe* files to
install software the Linux world uses "packaging systems". Red Hat Linux
and Mandriva use *.rpm* packages, Ubuntu, Debian, Mepis, Xandros use
*.deb* packages and Slackware/Vector use *.tgz* packages. |vector_edition|
also uses ".tlz" packages, which take less space and enable us to
provide more software on one CD. Many Linux packages are contained in
FTP repositories and software is available which automates the download
and installation of the correct package for your system in one easy
step. |vector_edition| uses a package management system based on *slapt-get*
(text mode) and *gslapt* (graphical, GUI mode).

The |vector_edition| packaging system is based around the Slackware packaging
system and consists of *.tgz* or *.tlz* packages. However, the Slackware
system has been improved by slapt-get. slapt-get mimics the Debian apt
packaging system. It automates the download, cataloguing and install
process and also provides some dependency checking which was not
available with the Slackware system. gslapt is a GUI-based wrapper
around slapt-get, so is the recommended packaging software for newcomers
to |vector_edition|. On the other hand if you are a confident command line
user you will find slapt-get, coupled with piping to other Linux
commands very powerful.

slapt-get and gslapt enable |vector_edition| users to easily download,
install, upgrade, remove, search, and query packages of software on
their system. You only really need need to do two steps to
install/upgrade or remove software packages :

* Update local information on packages available from your chosen
   source(s).
* Install, Upgrade or Remove the packages of your choice.

.. versionadded:: 6.0
    |vector_edition| 6.0 now includes by default the small daemon 
    `"slapt-update-notifier" 
    <http://software.jaos.org/#slapt-update-notifier>`_. This utility will 
    check the |vector_edition| repository and will pop-up a small notifier in 
    the bottom right applet section of either KDE or xfce to inform the user 
    when updates are available. If clicked gslapt will be launched. Please 
    ensure that you do wish to do this. Although VL packagers are very careful
    at their job there is no guarantee that software updates will not break 
    your system. You have been warned!

 
Update the Package List
=======================

This step downloads the necessary package information details from the
|vector_edition| repository (a specific FTP site). You must ensure your
Internet connection is working, then: 1) command line method:
``slapt-get --update`` , or 2) GUI method: launch "gslapt", click Update
icon. These commands can take a few seconds to a few minutes depending
on the speed of your Internet connection, the load on the server and the
speed of your system.


Install the Packages
====================

1) command line method: ``slapt-get --install [packagenames]`` , or 2)
gslapt method: launch gslapt, search for your required packages in the
list, click those you want, click Package Install. Remember that this
command actually fetches the package from a server and then installs it.
This can take some time depending on the speed of your Internet
connection, the load on the server and the speed of your system. Please
be patient.


Other useful slapt-get commands
===============================

Please refer to the documentation for slapt-get: ``man slapt-get``, the
HOWTOs at the VL Forum: `slapt-get
basics <http://forum.vectorlinux.com/index.php?topic=11.0>`_ and `gslapt
basics <http://forum.vectorlinux.com/index.php?topic=57.0>`_ or go to
`the slapt-get FAQ site for more advanced
topics. <http://software.jaos.org/BUILD/slapt-get/FAQ.html>`_

You may wish to try some of these slapt-get commands:

``slapt-get --available``
    to list all the available packages from the source repository
``slapt-get --installed``
    to list only those packages that you have already installed
``slapt-get --search [packagename(s)]``
    to search the listings for specific package(s)
``slapt-get --install [packagename(s)]``
    to install (or to upgrade an already installed) package(s)
``slapt-get --clean``
    to clear your temp directory of downloaded packages
``slapt-get --remove [packagename(s)]``
    to remove packages(s)
``slapt-get --show [package(s)]``
    to show a package(s) description 
``slapt-get --reinstall --install [package]``
    to reinstall an existing package
``slapt-get --reinstall --install [exactpackagenameandnumbers]``
    to downgrade a package
``slapt-get --available|sort|less``
    to show a sorted, paged list of available packages from the source
``slapt-get --installed|sort|less``
    to show a sorted, paged list of installed packages on your system
``slapt-get --available|grep inst=no|sort|less``
    to show a sorted, paged list of available, uninstalled packages
``slapt-get --available|grep fluxbox``
    to show only available packages related to e.g. fluxbox
``slapt-get --available|grep inst=no|grep vl5|sort|less``
    to show only available, uninstalled packages specifically packaged for VL5 
``slapt-get --available|grep fluxbox|awk '{print $1}'|sort|uniq|xargs -r slapt-get --install``
    to install all packages pertaining to e.g. fluxbox
``slapt-get --available|grep inst=no|awk '{print $1}'|sort|uniq|xargs -r slapt-get --install``
    to install every available package not yet installed (DANGER!)


Gslapt
======

*Gslapt* is the GUI software for managing |vector_edition| packages. It is
the software most likely to be used by newcomers to VL. It is a GUI
wrapper around typical slapt-get commands (see section above) and makes
slapt-get much easier unless you are very comfortable with the command
line. The thumbnails below illustrate the essential features you need to
know to operate gslapt effectively. Click on the thumbnails to see a
larger version.

The gslapt window is comprised of 5 sections, arranged vertically. From
top down these are the 1) menu panel, 2) icon panel, 3) search panel, 4)
package listing panel and 5) package description panel. In essence,
gslapt is a very user-friendly so we will not belabour this section.

.. warning::
    Please note that the gslapt command "Mark All Upgrades" followed by
    "Execute" will upgrade every installed package on your system for which
    there is a newer version at the repository. This is the equivalent of
    the slapt-get command ``slapt-get --upgrade``. *Either of these
    commands can be dangerous and may lead to a broken system - you have
    been warned!* See section below for further details.

.. figure:: ../_static/images/gslapt/gslapt_basic.png
   :alt: Basic
   :align: center

   Gslapt

.. figure:: ../_static/images/gslapt/gslapt_prefs_wd.png 
   :alt: Preferences
   :align: center

   Gslapt Preferences

.. figure:: ../_static/images/gslapt/gslapt_prefs_excludes.png 
   :alt: Excludes
   :align: center

   Excludes

.. figure:: ../_static/images/gslapt/gslapt_prefs_sources.png 
   :alt: Sources
   :align: center

   Sources

.. figure:: ../_static/images/gslapt/gslapt_prefs_search.png 
   :alt: Search
   :align: center

   Search

.. figure:: ../_static/images/gslapt/gslapt_prefs_install.png 
   :alt: Install
   :align: center

   Install


Known problems
==============

System Upgrades
---------------

Unfortunately at the moment, the |vector_edition| repository is not yet as
robust and debugged as a typical Debian package site (but it gets better
by the week)!. Consequently, it is highly recommended that users
**refrain from attempting full system upgrades.** In other words, do not
use the commands ``slapt-get --upgrade`` or "gslapt, Mark All Upgrades,
Execute". Either of these commands will upgrade *every* installed
package on your system for which there is a more recent version. It is
suggested that you ``slapt-get --install [packagenames]`` only for those
packages which you definetely require an upgrade for a functionality
improvement or to cure a serious security risk to the older package. At
present, it is wiser to actually reinstall a stable, newer release of
|vector_edition| than to attempt a system upgrade with *slapt-get --upgrade*.
Remember the old adage: "if it ain't broke don't fix it!". The VL
developers and packagers are working to improve our package repository
so that sometime in the near future full "safe" complete system upgrades
using slapt-get --upgrade will be a viable option.


Slackware Packages
------------------

|vector_edition| is able to install Slackware packages. However, Slackware
does not normally support slapt-get nor do Slackware packages do any
dependency checking. For this reason we do not recommend enabling any
Slackware repositories in the /etc/slapt-get/slapt-getrc configuration
file (or with gslapt, Edit, Preferences, Sources, Add a Source). The
default slapt-get configuration file will enable the user to download
and install Vector packages only. Slackware packages which are
non-system packages will probably work fine with VL but you may have to
install all the dependant packages first and there is no guarantee the
package will work properly. If you do decide to load a Slackware package
please ensure you download packages built for the correct version of
Slackware. For VL6.0 the Slackware version it is based upon is v.12.1

To install a Slackware package that you have downloaded you use the
Slackware "installpkg" command rather than slapt-get or gslapt (see
section below for more details).


Other Slackware Package Commands
================================

For the sake of completeness this section documents basic Slackware
Package commands which are also available to the VL user. Remember that
slapt-get (and gslapt) are built upon Slackware package commands. The
Slackware commands, unlike slapt-get and gslapt do not retrieve packages
from mirror sites: they assume you have already downloaded the
appropriate package. The commands available are:
``installpkg, removepkg, upgradepkg, explodepkg, makepkg and pkgtool.``
You will find a "man page" for each of these commands, e.g.
``man installpkg`` for a full explanation of how to use each of these
commands. Some examples of Slackware package commands usage:

*  to install a "leafpad" package which you downloaded from
   linuxpackages.net, first "cd" into the directory where the package is
   located and then: 
*  to remove an "abiword" package from your system:
   ``removepkg abiword``.
*  to extract the contents of a Slackware package into the current
   directory: ``explodepkg leafpad-0.8.4-i486-1arf.tgz``.
*  to install, remove or view packages from a handy menu system:
   ``pkgtool`` .

One of the most convenient features of "mc", the Midnight Commander
menu-based file manager, is that VL has added some Slackware package
commands to the "F2" menu. One may easily pack, install and remove
Slackware/Vector *.tgz* packages using mc in this manner. In the mc F2
menu you also can convert an RPM package to a Slackware package. This
facility is also available from the command line using the command:
``rpm2tgz``.

Furthermore, VL provides the convenient command: ``pkgtool`` . "pkgtool"
is the Slackware package utility - a command line wrapper around the
Slackware package commands mentioned in the paragraphs above, with a few
enhancements. By typing: ``pkgtool`` as root, you will see the options
available to you.


Building Packages
=================

Experienced Linux users who are familiar with compiling software from
source code are encouraged to contribute to the |vector_edition| community by
building packages which are not yet in the VL repositories. Your
contributions will enhance VL for all users by providing a more
comprehensive suite of packages for the VL community.

The procedure to build a proper |vector_edition| package is outlined at the
|vector_edition| Forum
`here <http://forum.vectorlinux.com/index.php?topic=1380.0>`_. Please
follow the procedures exactly as any deviations from this methodology
may result in non-robust package that may break a user's system.

VL now has a new application provided with 6.0 called "vpackager". This
newbie-friendly GUI software automates the compilation and creation of a
|vector_edition| package from source code. More details can be found
`here <http://code.google.com/p/vpackager/wiki/HowToUse>`_.

