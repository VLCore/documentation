========================================
Power Management Setup & Troubleshooting
========================================

Here are some things that can help a VectorLinux user troubleshoot their
power management options.  This is useful if you use VectorLinux on a
laptop and wish to use suspend/resume.

Preparations
============

Make sure the following packages are installed on your system.

- pm-utils
- hibernate-script
- upower (version 0.9.2x)

These are available via ``slapt-get`` or ``GSlapt`` from the xfce4 menu.


.. note::
   Version 0.99.x of upower no longer handles the suspend/hibernate stuff.
   Instead, it has handed that functionality over to ``systemd`` which, 
   which is not available or supported in |vector_edition|
  

WARNINGS:
=========
- The default setup hibernates to swap.  You may run into problems if you
  share your swap partition between more than one distro.


Troubleshooting
===============

If the required packages are installed, and you are still not able to suspend your
session, the following steps may help you find out the reason.  You will need to open
a terminal window and type the commands as written below.

Check the upower dump
---------------------
Query the upower daemon to see which PM capabilities it reports.
- In a terminal window, run ``upower -d``.  Reading the output should be pretty straight forward.

Check what dbus thinks
-----------------------
Query the system message bus to see what PM capabilities it reports.
 
``qdbus --system org.freedesktop.UPower /org/freedesktop/UPower org.freedesktop.UPower.CanSuspend``

``qdbus --system org.freedesktop.UPower /org/freedesktop/UPower org.freedesktop.UPower.CanHibernate``

Check the polkit authorization policies
---------------------------------------

Even if the system *can* suspend or hibernate, your user still needs permissions
to access that functionality.  This is where polkit comes in.  Check if the current
policy allows the currently logged in user to access the PM functionality.

Issue these commands as the non-root user(s) you want to allow to suspend/hibernate
the system.

``pcheck --action-id org.freedesktop.upower.suspend --process $$; echo $?``

Any returned value greater than 0 here means you are *NOT ALLOWED* to
perform the queried action.

``pcheck --action-id org.freedesktop.upower.hibernate --process $$; echo $?``

Any returned value greater than 0 here means you are *NOT ALLOWED* to
perform the queried action.

Adjusting the polkit rules
~~~~~~~~~~~~~~~~~~~~~~~~~~

If the commands in the above section report you do not have permission to execute 
the suspend or hibernate actions, make sure the following files exist and have the
correct content as specified below.

file: ``/etc/polkit-1/rules.d/01-default.rules``
::

  polkit.addRule(function(action,subject) {
      if (action.id == "org.freedesktop.upower.suspend" ||
          action.id == "org.freedesktop.upower.hibernate") {
          return polkit.Result.YES;
          }
  });

file: ``/etc/polkit-1/localauthority/50-local.d``
::

  [Local Users]
  Identity=unix-group:wheel
  Action=org.freedesktop.upower.*
  ResultAny=yes
  ResultInactive=no
  ResultActive=yes


Bootloader
~~~~~~~~~~
In addition to having support and permissions, your bootloader must also be instructed
to look for suspended sessions.  Not having the bootloader setup correctly will result
in not being able to resume a suspended session.

If you are using the ``GRUB2`` bootloader, edit your ``/etc/default/grub`` file and
add the following line:  ``GRUB_CMDLINE_LINUX="resume=/dev/your-swap-partition"``.  Of course, point it to the right device path for your swap partition.  After that you will
need to run ``grub-mkconfig -o /boot/grub/grub.cfg`` to re-generate your bootloader 
configuration and make the changes take effect.

If you are using the ``LiLO`` bootloader, you will need to edit your ``lilo.conf`` and 
find the section for the OS you want to suspend/resume.  You will need to either add an
``append=`` line or update the existing append line.  The append line should have this
in it:  ``resume=/dev/your-swap-partition"``.  Again, point it to the right device
path for your swap partition.  After you save the file, run ``lilo -v`` to write the
changes to your bootloader target and make the changes effective.

Testing Your Results
====================

To test your results, you need to just reboot (1 time to reload the polkit policies).
Your ``Menu -> Logout`` in xfce4 should have a 'Hibernate' and/or 'Suspend' button.
Click on either one of them.  The system should hibernate or suspend.  Then reboot
to resume.


Helpful Links
==============

http://www.slackwiki.com/Hibernate

https://forums.gentoo.org/viewtopic-t-977948-start-0.html
