
#####################################################
The |vector_edition| Filesystem and How to Find Files
#####################################################


Introduction
============

|vector_edition|, as with most other Linux distributions, tries to adhere to
the `Linux Filesystem Hierarchy
Standard. <http://www.pathname.com/fhs/>`_ This standard is necessary to
enable developers and users to install, move, place and find files in
locations that make sense and are consistent between distributions. This
document will not discuss the details of the Linux FHS as you can read
such details at the above website. What we wish to point out here are
the important directories and file locations for those configuration
files and directories which are particularly relevant or unique to
|vector_edition|. It is hoped this will enable newcomers to navigate the many
Linux directories more effectively and to know where to find files to
fix any problems that may arise. Secondly, we briefly discuss some of
the Linux utilities that will help you locate files/directories.

It also needs to be known that |vector_edition| is based upon the Slackware
Linux distribution. Slackware Linux uses the BSD-style file layout for
its system initialization script files. Consequently Slackware and Linux
do not use System V init scripts. This is an important fact if you wish
to navigate or edit the init configuration scripts. If interested, you
can `read about the
difference <http://www.linux.com/article.pl?sid=06/01/03/1728227>`_
between BSD and System V init scripts.

|vector_edition| provides *VasmCC* the "Vector Administration and Services
Menu" system administration utility amongst other utilities, plus a
fully configured Linux system for most common hardware. Thus it should
generally be unnecessary for newcomers to have to manually edit any of
the configuration files or init scripts.


Important Configuration Files
=============================

Most system configuration files are located in the */etc* directory.
Most user-specific configuration files are in the appropriate home
directory as a "hidden" "rc" file, e.g. *~/.bashrc* or within a
directory specific to the application, e.g. *~/.config/xfce4* (where ~
is a shortcut to your home directory). Navigate to the */etc* directory
in your favourite file manager application and we will analyze the most
important system config files:

-  */etc/lilo.conf* : the lilo boot loader configuration file
-  */boot/grub/menu.lst* : the grub bootloader configuration file - if
   you use grub as your bootloader.
-  */etc/fstab* : the block device (drives) system mounting file;
   controls which drives are mounted at bootup.
-  */etc/inittab* : configuration file for the prime "init" script;
   determines bootlevel
-  */etc/passwd* : user and password configuration file
-  */etc/X11/xorg.conf* : the X config file; controls keyboard, mouse,
   video card and monitor
-  */etc/slapt-get/slapt-getrc* : config file for slapt-get - the VL
   packaging tools
-  */etc/modules.conf* : config file for loading kernel modules
-  */etc/resolv.conf* : config file for network nameservers - set
   automatically if your ISP uses DHCP (most do).
-  */etc/samba/smb.conf* : Samba config file
-  */etc/skel* : "Skeleton" desktop environment base config files,
   copies to each users /home directory upon installation

The important BSD-style system initialization scripts which the
knowledgeable user may at some time wish to edit are:

-  */etc/rc.d* : config DIRECTORY containing most of the Slackware-style
   startup scripts
-  */etc/rc.d/README* : please do if you want to learn more!
-  */etc/rc.d/rc.S* : one of the first boot scripts to run to prepare
   your system at boot time.
-  */etc/rc.d/rc.modules* : modules to be loaded at boot time
-  */etc/rc.d/rc.inet1* : config file for eth0, you primary network
   card.
-  */etc/rc.d/rc.local* : local bootup customization script. Put your
   additions here.

Now the most important user config files (where ~ denotes your home
directory) are listed below. Note that almost every application you use
will probably create a *~/.application* file or directory where it
stores your preference settings.

-  *~/.bashrc* : config file for nonlogin, interactive bash shells
-  *~/.Xdefaults* : config file for terminal console programs such as
   xterm, mrxvt
-  *~/.kde* : kde preferences and autostart directories
-  *~/.config* : directory for xfce4 and lxde config files
-  *~/.jwmrc* : the jwm window manager config file (if JWM installed)
-  *~/.jwm* : the jwm window manager directory (if JWM installed)
-  *~/.fluxbox* : directory for the fluxbox window manager's config
   files (if fluxbox installed)
-  *~/.icewm* : directory for the IceWM window manager's config files
   (if IceWM installed)

And lastly here are some other important log files and other files which
you may need to refer to at some stage:

-  */var/log/packages* : directory containing description files for
   every Slackware/VL package installed on your system
-  */var/log/removed\_packages* : directory containing description files
   for every Slackware/VL package removed from your system
-  */home/ftp/pub/veclinux-6.0/packages* : directory containing all
   packages downloaded using gslapt or slapt-get. Also contains mirror
   package info.


How To Find Files
=================

GUI Tools
---------

Although not as efficient and fast as the command line tools listed
below some users will feel more comfortable using a GUI application to
help them find specific files. Depending upon the version of VL you have
installed look for one or more of these applications:

-  kfind: if KDE is loaded
-  gtkfind: if xfce loaded

slocate or locate
-----------------

``slocate`` or ``locate`` commands provides a super fast, secure way to
search an indexed database of every file on your system. However, you
must first of all create the database and keep it up-to-date by running
``updatedb`` as root. You can place updatedb in your weekly crontab
file. slocate is much faster and easier than using ``find``.
Examples of using slocate:

-  ``slocate *.pdf`` will search the slocate database for every pdf file
   on your system.
-  ``slocate kde | grep rc`` will search the slocate database (whose
   location is */var/lib/slocate/slocate.db*) for all files containing
   kde which also contain the phrase "rc".
-  ``slocate .png | less`` searches for all instances of files ending
   .png and sends the output to the less pager so that you can browse
   the list easily.

If you want to know where all the files were placed when you installed a
Vector Linux .tgz package then slocate is your best bet. You can also
browse this information at /var/log/packages.

mc (Midnight Commander)
-----------------------

``mc``, *Midnight Commander* is one of the most useful applications in
Linux. It is a very fast, text-based file manager, much more powerful
than many of the GUI file managers if learned properly - the only thing
you can't do is drag and drop. Quite frankly, you will double your
computing efficiency if you master this application. Playing with mc is
the quickest way to learn your way around the VL filesystem. Press F1
for extensive help and read ``man mc``.

find
----

``find`` is a very powerful, but newbie-unfriendly command line program
for finding files. Extensive options but a steep learning curve. If you
want learn more `read this
HOWTO. <http://www.linux-tutorial.info/modules.php?name=Tutorial&pageid=31>`_
Some examples:

-  ``find . -name 'a*'`` : will find all files beginning with the letter
   a in the current directory
-  ``find /etc -mtime -1`` : will find all files in the /etc directory
   modified within the past day
-  ``find . \( -name temp -o -name tmp \) -print -exec rm {} \;`` : will
   display and then delete all files in the current directory named temp
   or tmp.

whereis
-------

The ``whereis`` command locates the binary, source and manual pages for
a command, e.g.:
``whereis sort`` returns the output:

    sort: /usr/bin/sort /usr/man/man1/sort.1.gz /usr/man/man3/sort.3.gz
    /usr/share/man/man1/sort.1.gz /usr/share/man/man3/sort.3.gz

apropos
-------

The ``apropos`` command searches a database of all the manual (man)
pages and returns those page names for the string you provide, e.g.:
``apropos pdf`` returns:

::

    docbook2pdf [jw] (1) - (Jade Wrapper) converts SGML files to other formats
    dvipdf (1) - Convert TeX DVI file to PDF using ghostscript and dvips
    gs (1) - Ghostscript (PostScript and PDF language interpreter and previewer)
    gsnd (1) - Run ghostscript (PostScript and PDF engine) without display
    pdf2dsc (1) - generate a PostScript page list of a PDF document
    pdf2ps (1) - Ghostscript PDF to PostScript translator
    pdfopt (1) - Ghostscript PDF Optimizer
    ps2ascii (1) - Ghostscript translator from PostScript or PDF to ASCII
    ps2pdf (1) - Convert PostScript to PDF using ghostscript
    ps2pdf12 [ps2pdf] (1) - Convert PostScript to PDF 1.2 (Acrobat 3-and-later compatible) using ghostscript
    ps2pdf13 [ps2pdf] (1) - Convert PostScript to PDF 1.3 (Acrobat 4-and-later compatible) using ghostscript
    ps2pdfwr (1) - Convert PostScript to PDF without specifying CompatibilityLevel, using ghostscript
    texi2dvi4a2ps (1) - Compile Texinfo and LaTeX files to DVI or PDF
    tiff2pdf (1) - convert a TIFF image to a PDF document

The above output shows all the commands and a brief description from the
man page title of what they do.

which
-----

The ``which`` command searches the directories in the PATH variable and
displays the absolute pathname of the first file it finds whose simple
filename is the same as the command, e.g.: ``which sort`` returns:
*/usr/bin/sort*

whatis
------

The ``whatis`` command searches a set of database files containing short
descriptions of system commands for keywords and displays the result on
the standard output. Only complete word matches are displayed. Requires
that the "whatis database" is up to date. To do that run the command
"makewhatis" as root.

